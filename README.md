# panel Log

To show panels in console log.
It not work on VSCode output and some anothers IDEs.

## GIT

```
https://bitbucket.org/reytuty/panel-log/src/master/

``` 


## Output

```
  Application         Version             Time Running        Load                                                                                  
  My App              1.0.0               0:00                COMPLETE                                                                              
----------------------------------------------------------------------------------------------
  Size                Length              Total               Money                                                                                 
  10                  5                   15                  $ 510.37                                                                              
----------------------------------------------------------------------------------------------
[ITEM 0] x{1 value: 3 
[ITEM 1] x{2 value: 3 
[ITEM 2] x{3 value: 3 
[ITEM 3] x{4 value: 3 
[ITEM 4] x{5 value: 3 
----------------------------------------------------------------------------------------------

```

# Install

To install using npm

```

npm i  panel-log

```

## Git repository

https://github.com/reytuty/panel-log


# Instantate 

```
var panelLog = require('panel-log') ;
    panelLog.appName = "My App";
    panelLog.appVersion = "1.0.0";
    panelLog.start() ;
```

# Example 1

```
var pannelLog = require('panel-log') ;
    pannelLog.appName = "My App";
    pannelLog.appVersion = "1.0.0";
    pannelLog.start() ;
    //you can pass real things
    setTimeout(pannelLog.setPercentComplete, 500, 0.2);
    setTimeout(pannelLog.setPercentComplete, 1000, 0.4);
    setTimeout(pannelLog.setPercentComplete, 1500, 0.6);
    setTimeout(pannelLog.setPercentComplete, 2000, 0.8);
    setTimeout(pannelLog.setPercentComplete, 2500, 1);
    
    pannelLog.addLineGroup(1, "Title Here", [pannelLog.color.blue], "home") ;
    //make itens with method
    
    let count = 0;
    pannelLog.addItem(1, 0, (colorInfo)=>{
        if(count%2 == 0){
            colorInfo.color = [pannelLog.color.yellow];
        }
        return "Custom Label" ;
    }, 20, "fixed value", "home");
    pannelLog.addItem(1, 1, "Fixed Label", 35, (colorInfo)=>{
        colorInfo.color = [pannelLog.color.blue];
        return new Date();
    }, "home")

    pannelLog.registerChannel("h", "home") ;
    pannelLog.registerChannel("i", "internal") ;
    pannelLog.setChannel("home") ;

    //make lines with your own
    
    pannelLog.onUpdate.add(()=>{
        console.log("exemple 1") ;
        console.log("exemple 2") ;
        console.log("exemple 3") ;
        console.log("exemple 4") ;

    }) ;


```

# Example 2

```
var pannelLog = require('panel-log') ;
    pannelLog.appName = "My App";
    pannelLog.appVersion = "1.0.0";
    pannelLog.start() ;
    //you can pass real things
    setTimeout(pannelLog.setPercentComplete, 500, 0.2);
    setTimeout(pannelLog.setPercentComplete, 1000, 0.4);
    setTimeout(pannelLog.setPercentComplete, 1500, 0.6);
    setTimeout(pannelLog.setPercentComplete, 2000, 0.8);
    setTimeout(pannelLog.setPercentComplete, 2500, 1);
    //Your custom data
    pannelLog.onUpdate.add(()=>{
        //here is called every time screen update ecra
    }) ;
```