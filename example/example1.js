let pannelLog = require('../index') ;
    pannelLog.appName = "My App";
    pannelLog.appVersion = "1.0.0";
    pannelLog.start() ;
    //you can pass real things
    setTimeout(pannelLog.setPercentComplete, 500, 0.2);
    setTimeout(pannelLog.setPercentComplete, 1000, 0.4);
    setTimeout(pannelLog.setPercentComplete, 1500, 0.6);
    setTimeout(pannelLog.setPercentComplete, 2000, 0.8);
    setTimeout(pannelLog.setPercentComplete, 2500, 1);
    
    pannelLog.addLineGroup(1, "Title Here", [pannelLog.color.blue], "home") ;
    //make itens with method
    
    let count = 0;
    pannelLog.addItem(1, 0, (colorInfo)=>{
        if(count%2 == 0){
            colorInfo.color = [pannelLog.color.yellow];
        }
        return "Custom Label" ;
    }, 20, "fixed value", "home");
    pannelLog.addItem(1, 1, "Fixed Label", 35, (colorInfo)=>{
        colorInfo.color = [pannelLog.color.blue];
        return new Date();
    }, "home")

    pannelLog.registerChannel("h", "home") ;
    pannelLog.registerChannel("i", "internal") ;
    pannelLog.setChannel("home") ;

    //make lines with your own
    
    pannelLog.onUpdate.add(()=>{
        console.log("exemple 1") ;
        console.log("exemple 2") ;
        console.log("exemple 3") ;
        console.log("exemple 4") ;

    }) ;